﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Logic;
using Moq;
using Ninject;

namespace UnitTests
{
    class AccountOperationsTests
    {
        private Account _account;

        [SetUp]
        public void Init()
        {
            _account = new Account()
            {
                Id = 4,
                AccountNumber = "123",
                OwnerLastName = "Kowalski",
                OwnerFirstName = "Jan",
                AcceptableDebit = 10.0M,
                MoneyOnAccount = 130.0M,
            };
        }

        [TearDown]
        public void Clean()
        {
            _account = null;
        }

//        [Test]
//        public void Assert_when_list_of_all_acounts_is_uncorrect()
//        {
//            var accountRepo = new Mock<IAccountRepo>();
//
//            accountRepo.Setup(x => x.GetAll().ToList()).Returns(new List<Account>() { _account });
//
//            IKernel kernel = new StandardKernel(new DataAccessLayerModule());
//            kernel.Bind<IAccountOperations>().To<AccountOperations>();
//
//            AccountOperations operations = kernel.Get<AccountOperations>();
//
//            List<Account> listOfAllAccounts = new List<Account>(){_account};
//            
//            List<Account> listOfAllAccountsFromOperations = operations.GetListOfAllAccounts();
//
//            Assert.AreEqual(listOfAllAccounts,listOfAllAccountsFromOperations);
//        }

        [Test]
        public void Assert_when_withdraw_is_correct()
        {
            var accountRepo = new Mock<IAccountRepo>();

            accountRepo.Setup(x => x.Update(It.IsAny<Account>()));
            
            IKernel kernel = new StandardKernel(new DataAccessLayerModule());
            kernel.Bind<IAccountOperations>().To<AccountOperations>();
            
            AccountOperations operations = kernel.Get<AccountOperations>();

            operations.Withdraw(_account, 100);

            Assert.AreEqual(30.0M, _account.MoneyOnAccount);
        }

        [Test]
        public void Assert_when_remmittance_is_correct()
        {
            var accountRepo = new Mock<IAccountRepo>();

            accountRepo.Setup(x => x.Update(It.IsAny<Account>()));

            IKernel kernel = new StandardKernel(new DataAccessLayerModule());
            kernel.Bind<IAccountOperations>().To<AccountOperations>();

            AccountOperations operations = kernel.Get<AccountOperations>();

            operations.Remittance(_account, 100);

            Assert.AreEqual(230.0M, _account.MoneyOnAccount);
        }
    }
}
