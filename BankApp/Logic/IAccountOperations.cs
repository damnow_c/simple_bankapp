﻿using System.Collections.Generic;
using DataAccess;

namespace Logic
{
    public interface IAccountOperations
    {
        bool CreateNewAccount(string accountNumber, string lastName, string firstName, decimal possibleDebit);
        List<Account> GetListOfAllAccounts();
        void Remittance(Account account, decimal valueOfRemittance);
        void Withdraw(Account account, decimal valueOfWithdraw);
    }
}