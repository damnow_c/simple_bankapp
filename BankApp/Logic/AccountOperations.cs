﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using DataAccess;


namespace Logic
{
    public class AccountOperations : IAccountOperations
    {
        private readonly IAccountRepo _accountRepo;

        public AccountOperations(IAccountRepo accountRepo)
        {
            _accountRepo = accountRepo;
        }

        public bool CreateNewAccount(string accountNumber, string lastName, string firstName, decimal possibleDebit)
        {
            bool accountNumberExist = false;
            Account newAccount = new Account() {AccountNumber = accountNumber, OwnerLastName = lastName, OwnerFirstName = firstName, AcceptableDebit = possibleDebit + 1, MoneyOnAccount = 0};

            foreach (var existingAccount in GetListOfAllAccounts())
            {
                if (existingAccount.AccountNumber == accountNumber)
                {
                    accountNumberExist = true;
                }
            }

            if (accountNumberExist)
            {
                return false;
            }

            _accountRepo.Create(newAccount);
            return true;
        }

        public void Remittance(Account account, decimal valueOfRemittance)
        {
            account.MoneyOnAccount += valueOfRemittance;
            _accountRepo.Update(account);
        }

        public void Withdraw(Account account, decimal valueOfWithdraw)
        {
            if (valueOfWithdraw < account.MoneyOnAccount)
            {
                account.MoneyOnAccount -= valueOfWithdraw;
                _accountRepo.Update(account);
            }
        }

        public List<Account> GetListOfAllAccounts()
        {
            return _accountRepo.GetAll().ToList();
        }
    }
}
