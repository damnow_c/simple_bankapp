﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using DataAccess;

namespace Logic
{
    public class DataAccessLayerModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAccountRepo>().To<AccountRepo>();
        }
    }
}
