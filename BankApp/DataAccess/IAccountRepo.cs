﻿using System.Collections.Generic;

namespace DataAccess
{
    public interface IAccountRepo
    {
        void Create(Account account);
        Account GetByAccountNumber(string accountNummer);
        void Delete(Account account);
        IEnumerable<Account> GetAll();
        void Update(Account account);
    }
}