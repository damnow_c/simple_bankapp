﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess
{
    public class Account
    {
        [Index(IsUnique = true)]
        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerFirstName { get; set; }
        public decimal MoneyOnAccount { get; set; }
        public decimal AcceptableDebit { get; set; }
    }
}
