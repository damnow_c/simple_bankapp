﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataAccess
{
    public class BankDbContext : DbContext
    {
        public BankDbContext() : base("BankAppDbConnectionString")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<BankDbContext>());
            //Database.SetInitializer(new CreateDatabaseIfNotExists<GameDbContext>());
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<GameDbContext, Migrations.Configuration>());
        }

        public DbSet<Account> Accounts { get; set; }
    }
}
