﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class AccountRepo : IAccountRepo
    {
        private static BankDbContext _dbContext;
        public AccountRepo(BankDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Account account)
        {
            _dbContext.Accounts.Add(account);
            _dbContext.SaveChanges();
        }

        public static Account GetById(int id)
        {
            Account account = _dbContext.Accounts.Find(id);
            return account;
        }

        public Account GetByAccountNumber(string accountNumber)
        {
            Account account = _dbContext.Accounts.Find(accountNumber);
            return account;
        }

        public IEnumerable<Account> GetAll()
        {
            IEnumerable<Account> accounts = _dbContext.Accounts;
            return accounts;
        }

        public void Delete(Account account)
        {
            Account accauntToDelete = _dbContext.Set<Account>().Find(account.Id);

            if (accauntToDelete != null)
            {
                _dbContext.Set<Account>().Remove(account);
            }
        }

        public void Update(Account account)
        {
            var dbClient = _dbContext.Accounts.Find(account.Id);
            _dbContext.SaveChanges();
        }
    }
}
