﻿using DataAccess;

namespace BankApp_UserInterface
{
    public interface IMenuOptions
    {
        Account AccountFromAllAccounts();
        void CheckMoneyOnAccount();
        void CreateAccount();
        void Remittance();
        void WithDraw();
    }
}