﻿/*
1. Otwarcie rachunku
3. Wypłata gotówki
6. Debet na koncie (do pewnej wartości, po przekroczeniu zabrania na wykonywanie płatności)
7. Sprawdzenie salda

9. Wpłata gotówki

- warstwy
- DI
- testy
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using Ninject;

namespace BankApp_UserInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;

            IKernel kernel = new StandardKernel(new DataAccessLayerModule());
            kernel.Bind<IAccountOperations>().To<AccountOperations>();
            kernel.Bind<IMenuOptions>().To<MenuOptions>();

            MenuOptions menuOptions = kernel.Get<MenuOptions>();



            do
            {
                switch (InputOutputClass.menuBar())
                {
                    case 1: //Stwórz nowe konto
                        menuOptions.CreateAccount();
                        break;
                    case 2: //Wypłać pieniądze
                       menuOptions.WithDraw();
                        break;
                    case 3: //Wpłać pieniądze
                        menuOptions.Remittance();
                        break;
                    case 4: //Sprawdź stan konta
                        menuOptions.CheckMoneyOnAccount();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("\nŻegnaj!");
                        exit = true;
                        break;
                    default:
                        break;
                }

            } while (!exit);

        }
    }
}
