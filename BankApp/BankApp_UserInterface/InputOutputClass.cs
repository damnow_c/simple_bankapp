﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankApp_UserInterface
{
    class InputOutputClass
    {
        //example AskForData("Adres zakladki")
        public static string AskForData(string whatData)
        {
            Console.Write($"{whatData}: ");
            return (Console.ReadLine());
        }

        //example AskForValue("Wybor zakladki", 1, 110)
        public static int AskForIntFromTo(string whatData, int min, int max)
        {
            string data = "";
            int number = 0;
            Boolean success = false;
            do
            {
                Console.Write($"{whatData}: ");
                data = (Console.ReadLine());
                try
                {
                    number = Convert.ToInt32(data);
                    if (number >= min && number <= max)
                    {
                        success = true;
                    }
                }

                catch
                {
                    success = false;
                }

            } while (!success);

            return number;
        }

        public static decimal AskForDecimalFromTo(string whatData)
        {
            string data = "";
            decimal money = 0;
            Boolean success = false;
            do
            {
                Console.Write($"{whatData}: ");
                data = (Console.ReadLine());
                try
                {
                    money = Convert.ToDecimal(data);
                    if (money > 0)
                    {
                        success = true;
                    }
                }

                catch
                {
                    success = false;
                }

            } while (!success);

            return money;
        }

        public static int menuBar()
        {
            Console.Clear();
            int decyssion = 0;
            Console.WriteLine($"MENU\n1. Stwórz nowe konto.\n2. Wypłać pieniądze z wybranego konta.\n3. Wpłać pieniądze na wybrane konto.\n4. Wyświetl stan wybranego konta.\n5. Zakończ.");
            decyssion = AskForIntFromTo("\nWybierz opcję", 1, 5);
            return decyssion;
        }

        public static string AskForNumber(string whatData)
        {
            string number = "";
            bool success;

            do
            {
                success = true;

                Console.Write($"{whatData}: ");
                number = (Console.ReadLine());

                if (number.Length != 3)
                {
                    success = false;
                }

                foreach (char singleChar in number)
                {
                    if (48 > singleChar || singleChar > 57)
                    {
                        success = false;
                    }
                }

            } while (!success);

            return number;
        }
    }
}
