﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using Logic;

namespace BankApp_UserInterface
{
    public class MenuOptions : IMenuOptions
    {
        private readonly IAccountOperations _accountOperations;

        public MenuOptions(IAccountOperations accountOperations)
        {
            _accountOperations = accountOperations;
        }

        public void CreateAccount()
        {
            string accountNumber = "";
            string ownerLastName = "";
            string ownerFirstName = "";
            decimal acceptableDebit = 0;
            bool exit = true;

            Console.Clear();

            Console.WriteLine("Zakładanie nowego konta.\n\n");

            accountNumber = InputOutputClass.AskForNumber("Numer konta (3 cyfry)");
            ownerLastName = InputOutputClass.AskForData("Nazwisko właściciela");
            ownerFirstName = InputOutputClass.AskForData("Imię właściciela");
            acceptableDebit = InputOutputClass.AskForDecimalFromTo("Podaj możliwą wartość debetu na koncie");

            do
            {
                    if (_accountOperations.CreateNewAccount(accountNumber, ownerLastName, ownerFirstName, acceptableDebit))
                    {
                        Console.WriteLine("\nDodano nowego konto!");
                        exit = false;
                    }
                    else
                    {
                        Console.WriteLine("Niestety konto o danym numerze już istnieje.\n\n");
                        if (InputOutputClass.AskForIntFromTo("Czy chcesz powrócić do menu? (1. Powrót do menu. 2. Podanie innego numeru.)", 1, 2) == 1)
                        {
                            exit = false;
                        }
                        else
                        {
                            accountNumber = InputOutputClass.AskForNumber("Podaj inny numer konta (3 cyfry)");
                        }
                }
            } while (exit);

            Console.WriteLine("\n Naciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();

        }

        public Account AccountFromAllAccounts()
        {
            List<string> listOfNumbers = new List<string>();
            int i = 0;
            List<Account> listOfAllAccounts = _accountOperations.GetListOfAllAccounts();
            if (listOfAllAccounts.Count == 0)
            {
                Console.WriteLine("Brak kont w bazie danych!");
                return null;
            }
            else
            {
                foreach (var account in listOfAllAccounts)
                {
                    i++;
                    Console.WriteLine($"{i}. Numer konta: {account.AccountNumber} Nazwisko właściciela: {account.OwnerLastName}");
                }

                return listOfAllAccounts[InputOutputClass.AskForIntFromTo("Wybrane konto", 1, listOfAllAccounts.Count) - 1];
            }
        }

        public void WithDraw()
        {
            Console.WriteLine("Wypłacanie pieniędzy z konta.\n\n");

            Console.WriteLine("Wybierz konto z którego chcesz wypłacić pieniądze:\n");

            Account account = AccountFromAllAccounts();

            if (account != null)
            {
                decimal moneyWantedToWithDraw =
                    InputOutputClass.AskForDecimalFromTo($"Podaj ile pieniędzy chcesz wypłacić");
                if ( moneyWantedToWithDraw < account.MoneyOnAccount)
                {
                    _accountOperations.Withdraw(account, moneyWantedToWithDraw);
                }
                else
                {
                    if ( moneyWantedToWithDraw < account.MoneyOnAccount + account.AcceptableDebit)
                    {
                        if (InputOutputClass.AskForIntFromTo($"Czy chcesz wypłacić pieniądze wchodząc na debet? (1.Tak 2.Nie) ", 1, 2) == 1)
                        {
                            _accountOperations.Withdraw(account, moneyWantedToWithDraw);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Brak możliwych do wykorzystania srodków na koncie!");
                    }
                }
            }

            Console.WriteLine("\nNaciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();
        }

        public void Remittance()
        {
            Console.WriteLine("Wpłacanie pieniędzy na konto.\n\n");

            Console.WriteLine("Wybierz konto na które chcesz wpłacić pieniądze:\n");

            Account account = AccountFromAllAccounts();

            if (account != null)
            {
                decimal money =
                    InputOutputClass.AskForDecimalFromTo($"Podaj ile pieniędzy chcesz wpłacić");

                _accountOperations.Remittance(account, money);
            }

            Console.WriteLine("\nNaciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();
        }

        public void CheckMoneyOnAccount()
        {
            Console.WriteLine("Sprawdzanie środków na koncie.\n\n");

            Console.WriteLine("Wybierz konto którego wartość środków na koncie chcesz sprawdzić:\n");

            Account account = AccountFromAllAccounts();

            if (account != null)
            {
                Console.WriteLine($"Stan środków konta o numerze: {account.AccountNumber} wynosi: {account.MoneyOnAccount}");
            }

            Console.WriteLine("\nNaciśnij dowolny klawisz by przejść do menu.");
            Console.ReadKey();
        }
    }
}
